# Consultorías de software

## Nearsoft

- **Logotipo** <br /> ![Una imagen](imagenes/Nearsoft.jpg)
- **Nombre** <br /> Nearsoft
- **Sitio web** <br /> [Nearsoft](https://nearsoft.com/)
- **Ubicacion** <br /> 1. [Hermosillo Bulevar. Antonio Quiroga 21 Col. El Llano Hermosillo, Sonora, México 83210](https://www.google.com/maps/place/Blvd.+Antonio+Quiroga+21,+Quinta+Emilia,+83214+Hermosillo,+Son./@29.0975364,-111.0241336,17z/data=!3m1!4b1!4m5!3m4!1s0x86ce815ffe55796d:0x1957cc3fbb1473f3!8m2!3d29.0975364!4d-111.0219449) <br />
                       2. [Ciudad de México San Luis Potosí 196, 1er Piso Col. Roma Norte, Del. Cuauhtémoc Ciudad de México, México 06700](https://www.google.com/maps/place/San+Luis+Potos%C3%AD+196-1er+Piso,+Roma+Nte.,+Cuauht%C3%A9moc,+06700+Ciudad+de+M%C3%A9xico,+CDMX/@19.4129144,-99.1667886,17z/data=!3m1!4b1!4m5!3m4!1s0x85d1ff3edc236b2d:0x1d9383155604cc48!8m2!3d19.4129144!4d-99.1645999) <br />
                       3. [Chihuahua Calle 2da 2016 Col.Centro Chihuahua, Chihuahua, México 31000](https://www.google.com/maps/place/C.+Segunda+2016,+Bol%C3%ADvar,+Zona+Centro,+31000+Chihuahua,+Chih./@28.6320266,-106.0740368,17z/data=!3m1!4b1!4m5!3m4!1s0x86ea5caccf1dc1b1:0xa170910032bb6262!8m2!3d28.6320266!4d-106.0718481) <br />
                       4. [San Luis Potosi Sierra Ventana 268 Lomas 3a Sección San Luis Potosí, SLP, México 78216](https://www.google.com/maps/place/Sierra+Ventana+268-3a/@22.1399152,-101.0304558,17z/data=!4m6!1m2!2m1!1sSan+Luis+Potosi+Sierra+Ventana+268+Lomas+3a+Secci%C3%B3n+San+Luis+Potos%C3%AD,+SLP,+M%C3%A9xico+78216!3m2!1s0x842a9929ebebb253:0x1d12082afcc78a3a!15sCllTYW4gTHVpcyBQb3Rvc2kgU2llcnJhIFZlbnRhbmEgMjY4IExvbWFzIDNhIFNlY2Npw7NuIFNhbiBMdWlzIFBvdG9zw60sIFNMUCwgTcOpeGljbyA3ODIxNpIBEGNvbXBvdW5kX3NlY3Rpb24) <br />
                       5. [Mérida Calle 29 477 Col. Gonzalo Guerrero Mérida, Yucatán, México 97119](https://www.google.com/maps/place/C.+29+477,+Gonzalo+Guerrero,+97115+M%C3%A9rida,+Yuc./@21.0288852,-89.6263418,17z/data=!3m1!4b1!4m5!3m4!1s0x8f56769dea2b22ff:0xecabdc59260b0aa8!8m2!3d21.0288852!4d-89.6241531) <br />
- **Acerca de** <br /> Nearsoft tiene que ver con el desarrollo de software.
Nuestros equipos se autogestionan y entregan a tiempo.
Tratamos nuestro trabajo como un oficio y aprendemos unos de otros.
Constantemente subimos el listón.
- **Servicios** <br /> Desarrollo, las pruebas y la investigación y ejecución de UX / UI de software. 
- **Presencia** <br /> [Facebook](https://www.facebook.com/Nearsoft/) <br />
                       [Twitter](https://twitter.com/nearsoft?lang=es) <br />
                       [Instagram](https://www.instagram.com/nearsoft/?hl=es)
- **Ofertas laborales** <br /> [Ofertas laborales](https://nearsoft.com/join-us/)
- **Blog de ingenieria** <br /> [Blog de ingenieria](https://nearsoft.com/blog/)
- **Tecnologias** <br /> La metodología de desarrollo y SDLC general no cambia debido a la tecnología en uso. Además, tener experiencia en múltiples tecnologías de software nos da la posibilidad de recomendar alternativas que pueden aumentar sus posibilidades de éxito.



